 #!/usr/bin/python
 # -*- coding: utf-8 -*-
import socket, optparse, sys
from socketUtil import recv_msg, send_msg

#choisissez l’adresse avec l’option -a et le port avec -p
parser = optparse.OptionParser()
parser.add_option("-s", "--server", action="store_true", dest="serveur", default=False)
parser.add_option("-a", "--address", action="store", dest="address", default="localhost")
parser.add_option("-p", "--port", action="store", dest="port", type=int, default=1337)
opts = parser.parse_args(sys.argv[1:])[0]

if opts.serveur: #mode serveur
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serversocket.bind(("localhost", opts.port))
    serversocket.listen(5)
    print("Listening on port " + str(serversocket.getsockname()[1]))

    i=0
    while True:
        (s, address) = serversocket.accept()

        i += 1
        print (str(i) + "e connexion au serveur"))

        print("----------------------------------------------------------")

        modulo = Encryptor.getModulo()
        print("Envoie du modulo : {}".format(modulo))
        send_msg(serversocket, modulo)

        base = Encryptor.getBase()
        print("Envoie de la base : {}".format(base))
        send_msg(s, base)

        print("----------------------------------------------------------")

        privateKey = Encryptor.generateRandomInt(modulo)
        print("Clé privée : {}".format(privateKey))

        publicKey = Encryptor.generateKey(privateKey, base, modulo)
        






        s.close()

else: #mode client
    destination = (opts.address, opts.port)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(destination)

    modulo = recv_msg(s)
    print("Réception du modulo : {}".format(modulo))

    base = recv_msg(b)
    print("Réception de la base : {}".format(base))

    s.close()
